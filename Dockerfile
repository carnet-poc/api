FROM node:10-jessie

# Install default dependencies
RUN apt-get update && apt-get install -y curl \
	git

# 6.6.4 during the first setup
RUN npm i -g @nestjs/cli

COPY . /opt/app
WORKDIR /opt/app
